/*
The game logic  (no view code or direct user interaction)
The game is a simple guess or word game based on Mastermind
*/

#pragma once
#include <string>
#include <map>
#define TMap std::map


using FString = std::string;
using int32 = int;

//all values initialised to zero
struct FBullCowCount
{
	int32 Bulls = 0;
	int32 Cows = 0;
};

enum class EGuessStatus 
{
	Invalid_Status,
	OK,
	Not_Isogram,
	Wrong_Length,
	Wrong_Case
};

enum class EWordLengthStatus {
	Invalid_Status,
	OK,
	Too_Short,
	Too_Long
};



class FBullCowGame {
public:
	FBullCowGame();// constructor

	int32 GetCurrentTry() const;
	int32 GetMaxTries() const;
	int32 GetMinWordlength() const;
	int32 GetMaxWordlength() const;
	int32 GetHiddenWordLength() const;
	bool GetIsGameWon() const;


	EGuessStatus CheckGuessValidity(FString) const;


	void Reset();//bool?
	FBullCowCount SubmitValidGuess(FString);
	TMap<int32, FString> PopulateHiddenWords();
	EWordLengthStatus ChooseHiddenWord(int32);

private:
	//see constructor for init
	int32 MyCurrentTry;
	FString MyHiddenWord;
	TMap<int32, FString> MyHiddenWords;
	bool bGameIsWon;

	bool IsIsogram(FString) const;
	bool IsLowercase(FString) const;

};