#pragma once
#include "FBullCowGame.h"

//constants
const int32 MIN_WORD_LENGTH = 3;
const int32 MAX_WORD_LENGTH = 9;
//constructor
FBullCowGame::FBullCowGame() {
	Reset();
}


int32 FBullCowGame::GetHiddenWordLength() const { return MyHiddenWord.length(); }
int32 FBullCowGame::GetMinWordlength() const { return MIN_WORD_LENGTH; };
int32 FBullCowGame::GetMaxWordlength() const { return MAX_WORD_LENGTH; };
int32 FBullCowGame::GetCurrentTry() const {return MyCurrentTry;}
bool FBullCowGame::GetIsGameWon() const {return bGameIsWon;}

bool FBullCowGame::IsIsogram(FString Word) const
{
	//treat <2 letter words as isograms
	if (Word.length() < 2) { return true; }

	TMap<char,bool> LetterSeen;//setup Map
	//loop through word
	for (auto Letter : Word) {//range for, anon type
		Letter = tolower(Letter);//handle mixed case
		if (LetterSeen[Letter]) { 
			return false; 
		}//if letter is already in the map
		else 
		{
			LetterSeen[Letter] = true;//add to map
		}
			
	}
	return true; //e.g. in cases where /0 entered
}

bool FBullCowGame::IsLowercase(FString Word) const {

	for (auto Letter : Word) {
		if (!islower(Letter)) {
			return false;
		}
	}
	return true;
}


EGuessStatus FBullCowGame::CheckGuessValidity(FString Guess) const
{
	if (!IsIsogram(Guess)) //if the guess isn't an isogram
	{
		return EGuessStatus::Not_Isogram;
	}
	else if (!IsLowercase(Guess))//if guess isn't all lowercase
	{
		return EGuessStatus::Wrong_Case;
	}
	else if (Guess.length() != GetHiddenWordLength())//if guess length is wrong
	{
		return EGuessStatus::Wrong_Length;
	}
	else
	{
		return EGuessStatus::OK;
	}
}

void FBullCowGame::Reset() 
{
	MyCurrentTry = 1;
	bGameIsWon = false;
	MyHiddenWords = PopulateHiddenWords();
	int32 RandomWord = GetMinWordlength() + (rand() % static_cast<int32>(GetMaxWordlength() - GetMinWordlength() + 1));
	ChooseHiddenWord(RandomWord);
}

EWordLengthStatus FBullCowGame::ChooseHiddenWord(int32 Length) {
	EWordLengthStatus WordLengthStatus = EWordLengthStatus::Invalid_Status;
	if (Length < GetMinWordlength()) {
		WordLengthStatus = EWordLengthStatus::Too_Short;
	}else if(Length > GetMaxWordlength()) {
		WordLengthStatus = EWordLengthStatus::Too_Long;
	}
	else {
		MyHiddenWord = MyHiddenWords[Length];
		WordLengthStatus = EWordLengthStatus::OK;
	}
	return WordLengthStatus;
}


int32 FBullCowGame::GetMaxTries() const { 
	//return MyHiddenWord.length() + 3*((MyHiddenWord.length()/3)-1)+(MyHiddenWord.length()-1);
	TMap<int32, int32> WordLengthToMaxTries{ 
		{ 3,4 } ,
		{ 4,7 } ,
		{ 5,10 } ,
		{ 6,15 } ,
		{ 7,20 } ,
		{ 8,25} ,
		{ 9,30 } 
	};
	return WordLengthToMaxTries[MyHiddenWord.length()];
}


TMap<int32, FString> FBullCowGame::PopulateHiddenWords() {
	TMap<int32, FString> HiddenWords{
		{ 3,"bad" },
		{ 4,"easy" },
		{ 5,"right" },
		{ 6,"weight" },
		{ 7,"isogram" },
		{ 8,"orgasmic" },
		{ 9,"algorithm" }
	};
	return HiddenWords;
}


//counts Bulls and Cows and increasesa Try # when valid guess
FBullCowCount FBullCowGame::SubmitValidGuess(FString Guess)
{
	MyCurrentTry++ ;
	FBullCowCount BullCowCount;
	int32 GLength = Guess.length();

	//loop through the letters in the Hidden Word
	for (int32 HWChar = 0; HWChar < GetHiddenWordLength(); HWChar++){
		//compare against Guess
		for (int32 GChar = 0; GChar < GLength; GChar++) {
			//if they match then
			if (Guess[GChar] == MyHiddenWord[HWChar]){
				//increment Bulls if they're in the same place
				//increment cows if not
				if (HWChar == GChar) {
					BullCowCount.Bulls++;
				}
				else {
					BullCowCount.Cows++;
				}
			}
		}
	}
	if (BullCowCount.Bulls == GetHiddenWordLength()) {
		bGameIsWon = true;
	}
	return BullCowCount;
}

