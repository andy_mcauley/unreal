/*
This is the console that makes use of BullCow Class
This acts the view in an MVC pattern, and is responsible for all
user interaction.
For game logic see FBullCowGame
*/
#pragma once

#include<iostream>
#include<limits>
#include <string>
#include "FBullCowGame.h"

//to make syntax Uneal friendly
using FText = std::string;
using int32 = int;

//prototypes 
void PrintGameIntro();
void PlayGame();
FText GetValidGuess();
bool IsPlayAgain();
void ChooseWordLength();
void PrintGameSummary();


FBullCowGame BCGame; // instantiate a new game that we use across plays

int main() {

	do {
		PlayGame();
	} while (IsPlayAgain());

	return 0;

}


//introduce the game
void PlayGame()
{
	BCGame.Reset();
	PrintGameIntro();
	ChooseWordLength();
	while(!BCGame.GetIsGameWon() && BCGame.GetCurrentTry() <= BCGame.GetMaxTries()){
		FText Guess = GetValidGuess();
		FBullCowCount BullCowCount = BCGame.SubmitValidGuess(Guess);
		std::cout << "Bulls = " << BullCowCount.Bulls;
		std::cout << ", Cows = " << BullCowCount.Cows << "\n\n";
	}

	PrintGameSummary();

	return;
}
void PrintGameIntro() 
{
	std::cout << "\n\nWelcome to Bulls and Cows, a fun word game!" << std::endl;
	std::cout << std::endl;
	std::cout << "          }___{      ___ " << std::endl;
	std::cout << "          (o +)     (o 0) " << std::endl;
	std::cout << "   /-------\\ /       \\ /-------\\" << std::endl;
	std::cout << "  / | BULL |..       ..| COW  | \\" << std::endl;
	std::cout << " *  |-,----|           |------|  *" << std::endl;
	std::cout << "    ^      ^           ^      ^" << std::endl;
	std::cout << std::endl;
	std::cout << "An Isogram is a word that has no repeating letters " << std::endl;
	std::cout << "The object of this game is to guess the hidden isogram!" << std::endl; 
	std::cout << "(It's a random word between " << BCGame.GetMinWordlength() << " and " << BCGame.GetMaxWordlength() << " letters long)." << std::endl;
	std::cout << std::endl;
	std::cout << "Each letter of your guess that in the correct place scores a BULL."  << std::endl;
	std::cout << "Each letter of your guess that is in the word but a BULL scores a COW." << std::endl;
	std::cout << "If a letter of your guess is not in the word, it scores ZILCH!" << std::endl;
	std::cout << std::endl;
	return;
}

void ChooseWordLength() {

	std::cout << std::endl;
	std::cout << "Choose the length of the isogram you want to guess (between " << BCGame.GetMinWordlength() << " and " << BCGame.GetMaxWordlength() << " letters): ";
	std::string Response = "";
	int WordLengthChoice = 0;
	EWordLengthStatus WordLengthStatus = EWordLengthStatus::Invalid_Status;
	while (WordLengthStatus != EWordLengthStatus::OK) {
		//force numeric input
		while (!(std::cin >> WordLengthChoice)) {
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "Invalid input.  Try again! ";
		}
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		WordLengthStatus = BCGame.ChooseHiddenWord(WordLengthChoice);
		switch (WordLengthStatus)
		{
		case EWordLengthStatus::Too_Short:
			std::cout << WordLengthChoice << " letters is too short!";// Please enter a length between " << BCGame.GetMinWordlength() << " and " << BCGame.GetMaxWordlength() << "\n\n";
			break;
		case EWordLengthStatus::Too_Long:
			std::cout << WordLengthChoice << " letters is too long!";// Please enter a length between " << BCGame.GetMinWordlength() << " and " << BCGame.GetMaxWordlength() << "\n\n";
			break;
		case EWordLengthStatus::Invalid_Status:
			std::cout << Response[0] << " is not valid!\n\n";
			break;
		default:
			std::cout << "You entered: " << WordLengthChoice << "\n\n";
			break;
		}
	}
	std::cout << std::endl;
	std::cout << "OK! Can you guess the " << BCGame.GetHiddenWordLength() << " letter isogram I'm thinking of in " << BCGame.GetMaxTries() << " guesses?" << std::endl;
	std::cout << "SCORE " << BCGame.GetHiddenWordLength() << " BULLs to win!" << std::endl;
	std::cout << std::endl;
	return;
}


//loop till get a valid a guess from the player
FText GetValidGuess()
{
	EGuessStatus GuessStatus = EGuessStatus::Invalid_Status;
	FText Guess = "";
	do {
		std::cout << "Try " << BCGame.GetCurrentTry() << " of " << BCGame.GetMaxTries() << ": \nEnter your guess: ";
		std::getline(std::cin, Guess);
		//std::cin >> Guess;
		GuessStatus = BCGame.CheckGuessValidity(Guess);
		switch (GuessStatus)
		{
		case EGuessStatus::Wrong_Length:
			std::cout << "Please enter a " << BCGame.GetHiddenWordLength() << " letter word.\n\n";
			break;
		case EGuessStatus::Not_Isogram:
			std::cout << "Please enter an isogram (no repeating letters)!\n\n";
			break;
		case EGuessStatus::Wrong_Case:
			std::cout << "Please enter your word as all lowercase letters.\n\nof";
			break;
		default:
			break;
		}
		//std::cout << std::endl;

	} while (GuessStatus != EGuessStatus::OK);
	return Guess;


}

bool IsPlayAgain() {

	std::cout << std::endl;
	std::cout << "Do you want to play again? (Y/N): ";
	std::string Response = "";
	std::getline(std::cin, Response);
	return (Response[0] == 'Y' || Response[0] == 'y');

}

void PrintGameSummary(){
	if (BCGame.GetIsGameWon()) {
		std::cout << "WELL DONE - YOU WIN!\n\n";
	}
	else {
		std::cout << "Sorry you've used up all your tries - better luck next time!\n\n";
	}
	return;
}

